<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.landing');
});

Route::get('/w', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});

//Route Web
Route::get('/beranda', 'PostController@index');
Route::post('/post/create', 'PostController@create');
Route::get('/post/{post_id}', 'PostController@show');
Route::get('/post/{id}/edit', 'PostController@edit');
Route::put('/post/{id}/update', 'PostController@update');
Route::delete('/post/{post_id}', 'PostController@destroy');


// Route Komentar
Route::post('/komentar/create/{post_id}', 'KomentarController@create');
Route::get('/komentar/{id}/edit', 'KomentarController@edit');
Route::put('/komentar/{id}/update', 'KomentarController@update');
Route::delete('/komentar/{post_id}', 'KomentarController@destroy');
// Route::get('/post/{post_id}/{posts_id}', 'KomentarController@show');

//route follow
Route::get('/friends','FollowingController@alluser');
Route::get('/friends/{id}','FollowingController@following');

// route like
Route::get('post/like/{id}','LikeController@likepost');
Route::get('post/unlike/{id}','LikeController@unlikepost');
Route::get('komentar/like/{id}','LikeController@likecomment');
Route::get('komentar/unlike/{id}','LikeController@unlikecomment');

//Route post
Route::get('/profil', 'ProfileController@index');
Route::get('/profil/create', 'ProfileController@create');
Route::post('/profil/create', 'ProfileController@store');
Route::get('/profil/{id}/edit', 'ProfileController@edit');
Route::put('/profil/{id}/update', 'ProfileController@update');

<?php

namespace App\Http\Controllers;

use RealRashid\SweetAlert\Facades\Alert;
use App\Komentar;
use App\Post;
use Illuminate\Http\Request;
use Auth;
// use Session;

class KomentarController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    // public function index()
    // {
    //     $post = Komentar::all();
    //     return view('post.post', compact('post'));
    // }

    public function create(Request $request, $post_id)
    {
        $request->validate([
            'isi' => 'required',
        ]);
        $pos = Post::find($post_id);

        $komentar = new Komentar;
        $komentar->isi = $request["isi"];
        $komentar->post()->associate($pos);
        $komentar->users_id = Auth::id();
        $komentar->save();

        return redirect()->back();
    }

    public function edit($id)
    {

        $komentar = Komentar::find($id);
        

        return view('komentar.edit', compact('komentar'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'isi' => 'required',
        ]);
        

        $update = Komentar::where('id', $id)->update([
            'isi' => $request["isi"]
        ]);

        return redirect()->back();
    }

    public function show($id)
    {
        $komentator = Komentar::where('posts_id',$id)->first();

        return view('komentar.show', compact('komentator'));
    }

    public function destroy($id)
    {
        Komentar::destroy($id);

        return redirect()->back();
    }
}

<?php

namespace App\Http\Controllers;

use RealRashid\SweetAlert\Facades\Alert;
use App\Post;
use App\Following;
// use App\Komentar;
use Illuminate\Http\Request;
use Auth;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $post = Post::all();
        $following = Following::where('users_id','=', Auth::user()->id)
                    ->join('users','users.id','=','following_user_id')
                    ->where('status',1)
                    ->get();
        
        $followers = Following::where('following_user_id','=', Auth::user()->id)
                    ->join('users','users.id','=','users_id')
                    ->where('status',1)
                    ->get();

        return view('post.post', compact('post','following','followers'));
    }

    public function create(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'isi' => 'required',
        ]);

        $post = new Post;
        $post->judul = $request["judul"];
        $post->isi = $request["isi"];
        $post->users_id = Auth::id();
        $post->save();

        Alert::success('Postingan dibuat', 'Buat postingan berhasil');

        return redirect('/beranda');

    }

    public function edit($id)
    {

        $post = Post::find($id);

        return view('post.edit', compact('post'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'isi' => 'required',
        ]);
        

        $update = Post::where('id', $id)->update([
            'judul' => $request["judul"],
            'isi' => $request["isi"]
        ]);

        Alert::success('Postingan Terupdate', 'Update postingan berhasil');

        return redirect('/beranda');
    }

    public function show($id)
    {
        $post = Post::find($id);
        $komentar = $post->comments;
        // $komentator = Komentar::where('posts_id',$id)->first();
        return view('post.detail', compact('post'), compact('komentar'));
    }

    public function destroy($id)
    {
        Post::destroy($id);

        Alert::info('Postingan Terhapus', 'Hapus postingan berhasil');

        return redirect('/beranda');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Following;
use Auth;

class FollowingController extends Controller
{
    //
    public function alluser(){
        $allusers = User::where('id','!=', Auth::user()->id)->get();
        return view('pages.friends', compact('allusers'));
    }

    public function following($id){
        $follow = new  Following;
        $follow->users_id = Auth::user()->id;
        $follow->following_user_id = $id;
        $follow->status=1;
        $follow->save();
        
        // $following = Following::where('users_id','=', Auth::user()->id)
        //             ->join('users','users.id','=','following_user_id')
        //             ->where('status',1)
        //             ->get();

        return redirect('/friends');
    }

    public function followinguser(){
        $following = Following::where('users_id','=', Auth::user()->id)
                    ->join('users','users.id','=','following_user_id')
                    ->where('status',1)
                    ->get();
        return view(compact('following'));
    }
}

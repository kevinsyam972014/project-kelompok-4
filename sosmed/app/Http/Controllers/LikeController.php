<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Likepost;
use App\likecomment;
use Auth;

class LikeController extends Controller
{
    //
    public function likepost($id){
        // $like = Post::find($id);

        Likepost::create([
            'posts_id'=>$id,
            'users_id'=>Auth::id()
        ]);
        return redirect()->back();
    }
    
    public function unlikepost($id){
        $like = Likepost::where('posts_id',$id)->where('users_id',Auth::id())->first();
        $like->delete();

        return redirect()->back();
    }
    public function likecomment($id){
        // $like = Post::find($id);

        Likecomment::create([
            'komentar_id'=>$id,
            'users_id'=>Auth::id()
        ]);
        return redirect()->back();
    }
    
    public function unlikecomment($id){
        $like = Likecomment::where('komentar_id',$id)->where('users_id',Auth::id())->first();
        $like->delete();

        return redirect()->back();
    }
}

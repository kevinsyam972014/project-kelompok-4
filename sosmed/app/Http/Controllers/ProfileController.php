<?php

namespace App\Http\Controllers;

use App\User;
use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $profile = Profile::where('users_id','=', Auth::user()->id)->get();
        return view('profile.profil', compact('profile'));
    }

    public function create()
    {
        return view('profile.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'fullname' => 'required',
            'phone' => 'required',
            'ttl' => 'required',
            'foto_profil' => 'required|mimes:jpeg,png,jpg|max:1024'
        ]);

        $nm = $request->foto_profil;
        $namaFile = $request->foto_profil->getClientOriginalName() . '-' . time() . '.' .                         $request->foto_profil->extension();

        $profile = new Profile;
        $profile->fullname = $request["fullname"];
        $profile->phone = $request["phone"];
        $profile->ttl = $request["ttl"];
        $profile->foto_profil = $namaFile;
        $profile->users_id = Auth::user()->id;
 
        $nm->move(public_path().'/image', $namaFile);
        $profile->save();

        Alert::success('Success', 'Buat profile berhasil');

        return redirect('/profil');
    }

    public function edit($id)
    {

        $profile = Profile::find($id);

        return view('profile.edit', compact('profile'));
    }

    public function update($id, Request $request)
    {   
        $change = Profile::findorfail($id);
        $before = $change->foto_profil;
            
        $profile = [
            'fullname' => $request['fullname'],
            'phone' => $request['phone'],
            'ttl' => $request['ttl'],
            'foto_profil' => $before
        ];

        if($request->hasFile('foto_profil')){
            $request->foto_profil->move(public_path().'/image', $before);
       }

        $change->update($profile);

        Alert::success('Update Berhasil', 'Update data berhasil');
        return redirect('/profil');
    }
}

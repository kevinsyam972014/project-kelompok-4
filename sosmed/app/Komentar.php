<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Komentar extends Model
{
    //
    protected $table = 'komentar';
    protected $fillable = ['isi'];

    public function post(){
        return $this->belongsTo('App\Post', 'posts_id');
    }
    public function coment(){
        return $this->belongsTo('App\User', 'users_id');
    }
    public function likes(){
        return $this->hasMany('App\Likecomment','komentar_id');
    }
    public function is_liked_by_auth_user_comment(){
        $id= Auth::id();
        
        $liker=array();

        foreach($this->likes as $like):
            array_push($liker, $like->users_id);
        endforeach;

        if(in_array($id, $liker)){
            return true;
        }else{
            return false;
        }

    }
}

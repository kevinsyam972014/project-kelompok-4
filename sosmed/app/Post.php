<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Post extends Model
{
    protected $table = 'posts';
    // protected $fillable = ['judul', 'isi'];
    protected $guarded = [];

    public function author(){
        return $this->belongsTo('App\User', 'users_id');
    }
    public function comments(){
        return $this->hasMany('App\Komentar', 'posts_id');
    }
    public function likes(){
        return $this->hasMany('App\Likepost', 'posts_id');
    }
    public function is_liked_by_auth_user(){
        $id= Auth::id();
        
        $liker=array();

        foreach($this->likes as $like):
            array_push($liker, $like->users_id);
        endforeach;

        if(in_array($id, $liker)){
            return true;
        }else{
            return false;
        }

    }
    // public function commentator(){
    //     return $this->hasManyThrough('App\Komentar','App\User', 'posts_id','');
    // }
}

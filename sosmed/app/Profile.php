<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Profile extends Model
{
    protected $table = 'profile';
    protected $fillable = ['fullname', 'phone', 'ttl', 'foto_profil', 'users_id'];

    public function users()
    {
        return $this->belongsTo('App\User');
    }
    
}

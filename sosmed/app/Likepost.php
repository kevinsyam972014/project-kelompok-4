<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Likepost extends Model
{
    //
    protected $table = 'like_posts';
    protected $guarded = [];

    public function comments(){
        return $this->belongsTo('App\Post', 'posts_id');
    }

    public function user(){
        return $this->belongsTo('App\User', 'users_id');
    }


}

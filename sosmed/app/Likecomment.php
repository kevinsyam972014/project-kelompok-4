<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Likecomment extends Model
{
    //
    protected $table = 'like_komentar';
    protected $guarded = [];

    public function comments(){
        return $this->belongsTo('App\Post', 'komentar_id');
    }

    public function user(){
        return $this->belongsTo('App\User', 'users_id');
    }
}

@extends('pages.master')
@section('content')
    <div class="post-project">
        <h3>My Profile</h3>
        <div class="post-project-fields">
            <form action="" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-lg-12">
                        <input type="text" name="fullname" placeholder="Nama Lengkap">
                    </div>

                    <div class="col-lg-12">
                        <input type="text" name="phone" placeholder="No Telp">
                    </div>

                    <div class="col-lg-12">
                        <input type="date" name="ttl" placeholder="Tanggal Lahir">
                    </div>

                    <div class="col-lg-12">
                        <input type="file" name="foto_profil" placeholder="Foto Profil">
                    </div>

                    <div class="col-lg-12">
                        <ul>
                            <li><button class="active" type="submit" value="post">Post</button></li>
                            <li><a href="/beranda">Cancel</a></li>
                        </ul>
                    </div>
                </div>
            </form>
@endsection
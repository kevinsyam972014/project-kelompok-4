@extends('pages.master')
@section('content')
    <div class="post-project">
        <h3>Edit Profile</h3>
        <div class="post-project-fields">
            <form action="/profil/{{ $profile->id }}/update" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-lg-12">
                        <input type="text" name="fullname" placeholder="Nama Lengkap" value="{{ $profile->fullname }}">
                    </div>

                    <div class="col-lg-12">
                        <input type="text" name="phone" placeholder="No Telp" value="{{ $profile->phone }}">
                    </div>

                    <div class="col-lg-12">
                        <input type="date" name="ttl" placeholder="Tanggal Lahir" value="{{ $profile->ttl }}">
                    </div>

                    <div class="col-lg-12">
                        <input type="file" name="foto_profil" placeholder="Foto Profil" value="{{ $profile->foto_profil }}">
                        <img src="{{asset('image/'. $profile->foto_profil)}}" height="100px">
                    </div>
                    

                    <div class="col-lg-12">
                        <ul>
                            <li><button class="active" type="submit" value="post">Post</button></li>
                            <li><a href="/beranda">Cancel</a></li>
                        </ul>
                    </div>
                </div>
            </form>
@endsection
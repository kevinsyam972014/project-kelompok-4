@extends('pages.master')
@section('content')
    <section class="companies-info">
			<div class="container">
				<div class="company-title">
					<h3>All Users</h3>
				</div><!--company-title end-->
				<div class="companies-list">
					<div class="row">
                        @forelse ($allusers as $friend)
                            <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                                <div class="company_profile_info">
                                    <div class="company-up-info">
                                        <img src="http://via.placeholder.com/91x91" alt="">
                                        <h3>{{$friend->name}}</h3>
                                        <h4>User biasa</h4>
                                        <ul>
                                            <?php $if_null = App\Following::where('following_user_id','=', $friend->id)->get();
                                             if(is_null($if_null)){
                                             ?>
                                            <li><a href="/friends/{{$friend->id}}" title="" class="follow">Follow</a></li>
                                            <?php }
                                            else{ ?>
                                            <li><a href="/friends/{{$friend->id}}" title="" class="follow">Followed</a></li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div><!--company_profile_info end-->
                            </div>
                            @empty
                            Tidak ada kawan :(
                        @endforelse
                        
						
					</div>
				</div><!--companies-list end-->
				<div class="process-comm">
					<div class="spinner">
						<div class="bounce1"></div>
						<div class="bounce2"></div>
						<div class="bounce3"></div>
					</div>
				</div>
			</div>
		</section><!--companies-info end-->
@endsection
<header>
    <div class="container">
        <div class="header-data">
            <div class="search-bar">
                <form>
                    <input type="text" name="search" placeholder="Search...">
                    <button type="submit"><i class="la la-search"></i></button>
                </form>
            </div>
            <!--search-bar end-->
            <nav>
                <ul>
                    <li>
                        <a href="/beranda" title="">
                            <span><img src="{{ asset('images/icon1.png') }}" alt=""></span>
                            Home
                        </a>
                    </li>
                 
                    <li>
                        <a href="/profil" title="">
                            <span><img src="{{ asset('images/icon4.png') }}" alt=""></span>
                            Profiles
                        </a>
                       
                    </li>

                    <li>
                        <a href="/friends" title="">
                            <span><img src="{{ asset('images/icon4.png') }}" alt=""></span>
                            Find a Friend
                        </a>
                       
                    </li>
        
                </ul>
            </nav>
            <!--nav end-->
            <div class="menu-btn">
                <a href="#" title=""><i class="fa fa-bars"></i></a>
            </div>
            <!--menu-btn end-->
            <div class="user-account">
                <div class="user-info">
                    <img src="http://via.placeholder.com/30x30" alt="">
                    <a href="#" title="">{{ Auth::user()->name }}</a>
                    <i class="la la-sort-down"></i>
                </div>
                <div class="user-account-settingss">
                    <?php $if_null = App\Profile::where('users_id', '=', Auth::user()->id)->first();
                        if(is_null($if_null)){
                        ?>
                            <h3><a href="/profil/create">Account Profile</a></h3>
                        <?php }
                        else {
                        ?>
                            <h3>Account Profile</h3>
                        <?php } ?>
                        
                    
                    
                    {{-- <ul class="us-links">
                        <li><a href="profile-account-setting.html" title="">Account Setting</a></li>    
                    </ul> --}}
                    <h3 class="tc"><a href="{{route('logout')}}" onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}</a></h3>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    

                    
                                    {{-- <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form> --}}
                </div>
                <!--user-account-settingss end-->
            </div>
        </div>
        <!--header-data end-->
    </div>
</header>
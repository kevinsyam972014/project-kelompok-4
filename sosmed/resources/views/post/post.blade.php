@extends('pages.master')
@section('content')
<div class="row">
    <div class="col-lg-3 col-md-4 pd-left-none no-pd">
        <div class="main-left-sidebar no-margin">
            <div class="user-data full-width">
                <div class="user-profile">
                    <div class="username-dt">
                        <div class="usr-pic">
                            <img src="http://via.placeholder.com/100x100" alt="">
                        </div>
                    </div>
                    <!--username-dt end-->
                    <div class="user-specs">
                        <h3>{{ Auth::user()->name }}</h3>
                        <span>Graphic Designer at Self Employed</span>
                    </div>
                </div>
                <!--user-profile end-->
                <ul class="user-fw-status">
                    <li>
                        <h4>Following</h4>
                        <span>{{$following->count()}}</span>
                    </li>
                    <li>
                        <h4>Followers</h4>
                        <span>{{$followers->count()}}</span>
                    </li>
                    <li>
                        <a href="#" title="">View Profile</a>
                    </li>
                </ul>
            </div>
            <!--user-data end-->
        </div>
        <!--main-left-sidebar end-->
    </div>
    <div class="col-lg-6 col-md-8 no-pd">
        <div class="main-ws-sec">
            <div class="post-topbar">
                <div class="user-picy">
                    <img src="http://via.placeholder.com/100x100" alt="">
                </div>
                <div class="post-st">
                    <ul>
                        <li><a class="post-jb active" href="#" title="">Make a Post</a></li>
                    </ul>
                </div>
                <!--post-st end-->
            </div>
<div class="posts-section">
    <div class="post-bar">
        @foreach ($post as $value)
        <div class="post_topbar">
            <div class="usy-dt">
                <img src="http://via.placeholder.com/50x50" alt="">
                <div class="usy-name">
                    <h3>{{$value->author->name}}</h3>
                    <span><img src="images/clock.png" alt="">3 min ago</span>
                </div>
            </div>
            <div class="ed-opts">
                <a href="#" title="" class="ed-opts-open"><i
                        class="la la-ellipsis-v"></i></a>
                <ul class="ed-options">
                    <li><a href="/post/{{$value->id}}/edit" title="">Edit Post</a></li>
                    <li><a href="/post/{{$value->id}}" title="">Show Post</a></li>
                    <form action="/post/{{$value->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button class="bg-transparent border-0 hover" type="submit"><li><a>Delete Post</a></li></button>
                    </form>
                </ul>
            </div>
        </div>
        <div class="job_descp">
            <h3>{{ $value->judul }}</h3>
            <p>{!! $value->isi !!}</p>
        </div>
        <div class="job-status-bar">
            <ul class="like-com">
                @if ($value->is_liked_by_auth_user())
                    <li>
                        <a href="post/unlike/{{$value->id}}"><i class="la la-heart"></i> Dislike</a>
                        <span style="margin-left: 5px;">{{$value->likes->count()}}</span>
                    </li>
                    @else
                    <li>
                        <a href="post/like/{{$value->id}}"><i class="la la-heart"></i> Like</a>
                        <span style="margin-left: 5px;">{{$value->likes->count()}}</span>
                    </li>
                @endif
                
                <li><a href="/post/{{$value->id}}" title="" class="com"><img src="images/com.png"
                alt=""> Comments</a></li>
            </ul>
        </div>
        @endforeach
    </div>
    <!--post-bar end-->
</div>
@endsection
@section('create-post')
<div class="post-popup job_post">
    <div class="post-project">
        <h3>Buat Postingan</h3>
        <div class="post-project-fields">
            <form action="/post/create" method="POST">
                @csrf
                <div class="row">
                    <div class="col-lg-12">
                        <input type="text" name="judul" placeholder="Judul">
                    </div>

                    <div class="col-lg-12">
                        <textarea name="isi" class="form-control my-editor">{!! old('isi', $isi ?? '') !!}</textarea>
                    </div>

                    <div class="col-lg-12">
                        <ul>
                            <li><button class="active" type="submit" value="post">Post</button></li>
                            <li><a href="/beranda" title="">Cancel</a></li>
                        </ul>
                    </div>
                </div>
            </form>
        </div>
@endsection
@section('right-sidebar')
<div class="col-lg-3">
    <div class="right-sidebar"> 
        <div class="widget">
            <div class="sd-title">
                <h3>Suggestions</h3>
                <i class="la la-ellipsis-v"></i>
            </div>
            <!--sd-title end-->
            <div class="suggestions-list">
                <div class="suggestion-usd">
                    <img src="http://via.placeholder.com/35x35" alt="">
                    <div class="sgt-text">
                        <h4>Jessica William</h4>
                        <span>Graphic Designer</span>
                    </div>
                    <span><i class="la la-plus"></i></span>
                </div>
                <div class="suggestion-usd">
                    <img src="http://via.placeholder.com/35x35" alt="">
                    <div class="sgt-text">
                        <h4>John Doe</h4>
                        <span>PHP Developer</span>
                    </div>
                    <span><i class="la la-plus"></i></span>
                </div>
                <div class="suggestion-usd">
                    <img src="http://via.placeholder.com/35x35" alt="">
                    <div class="sgt-text">
                        <h4>Poonam</h4>
                        <span>Wordpress Developer</span>
                    </div>
                    <span><i class="la la-plus"></i></span>
                </div>
                <div class="suggestion-usd">
                    <img src="http://via.placeholder.com/35x35" alt="">
                    <div class="sgt-text">
                        <h4>Bill Gates</h4>
                        <span>C & C++ Developer</span>
                    </div>
                    <span><i class="la la-plus"></i></span>
                </div>
                <div class="suggestion-usd">
                    <img src="http://via.placeholder.com/35x35" alt="">
                    <div class="sgt-text">
                        <h4>Jessica William</h4>
                        <span>Graphic Designer</span>
                    </div>
                    <span><i class="la la-plus"></i></span>
                </div>
                <div class="suggestion-usd">
                    <img src="http://via.placeholder.com/35x35" alt="">
                    <div class="sgt-text">
                        <h4>John Doe</h4>
                        <span>PHP Developer</span>
                    </div>
                    <span><i class="la la-plus"></i></span>
                </div>
                <div class="view-more">
                    <a href="" title="">View More</a>
                </div>

            </div>
            <!--right-sidebar end-->
        </div>
    </div>
</div>
@endsection
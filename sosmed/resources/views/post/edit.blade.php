@extends('pages.master')
@section('content')
    <div class="post-project">
        <h3>Edit Postingan</h3>
        <div class="post-project-fields">
            <form action="/post/{{ $post->id }}/update" method="POST">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-lg-12">
                        <input type="text" name="judul" placeholder="Judul" value="{{ $post->judul }}">
                    </div>

                    <div class="col-lg-12">
                        <textarea name="isi" class="form-control my-editor">{!! old('isi', $post->isi ?? '') !!}</textarea>
                    </div>

                    <div class="col-lg-12">
                        <ul>
                            <li><button class="active" type="submit" value="post">Post</button></li>
                            <li><a href="/beranda">Cancel</a></li>
                        </ul>
                    </div>
                </div>
            </form>
@endsection
@extends('pages.master')
@section('content')
<div class="posty">
    <div class="post-bar no-margin">
        <div class="post_topbar">
            <div class="usy-dt">
                <img src="http://via.placeholder.com/50x50" alt="">
                <div class="usy-name">
                    <h3>{{$post->author->name}}</h3>
                    <span><img src="images/clock.png" alt="">3 min ago</span>
                </div>
            </div>
            <div class="ed-opts">
                <a href="#" title="" class="ed-opts-open"><i
                        class="la la-ellipsis-v"></i></a>
                <ul class="ed-options">
                    <li><a href="/post/{{$post->id}}/edit" title="">Edit Post</a></li>
                    <form action="/post/{{$post->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button class="bg-transparent border-0 hover" type="submit"><li><a>Delete Post</a></li></button>
                    </form>
                </ul>
            </div>
        </div>
        <div class="job_descp">
            <h3>{{ $post->judul }}</h3>
            <p>{!! $post->isi !!}</p>

        </div>
        <div class="job-status-bar">
            <ul class="like-com">
                <li>
                    @if ($post->is_liked_by_auth_user())
                        <li>
                            <a href="/post/unlike/{{$post->id}}"><i class="la la-heart"></i> Dislike</a>
                            <span style="margin-left: 5px;">{{$post->likes->count()}}</span>
                        </li>
                        @else
                        <li>
                            <a href="/post/like/{{$post->id}}"><i class="la la-heart"></i> Like</a>
                            <span style="margin-left: 5px;">{{$post->likes->count()}}</span>
                        </li>
                    @endif
                </li> 
                <li><a href="#" title="" class="com"><img src="images/com.png" alt=""> Comments</a></li>
            </ul>
        </div>
    </div><!--post-bar end-->
    <div class="comment-section">
        <div class="plus-ic">
            <i class="la la-plus"></i>
        </div>
        <div class="comment-sec">
            <ul>
                @forelse ($komentar as $value)
                    <li>
                        <div class="comment-list">
                            <div class="bg-img">
                                <img src="http://via.placeholder.com/40x40" alt="">
                            </div>
                            <div class="comment">
                                <h3>anonymous {{$value->users_id}}</h3>
                                <span><img src="images/clock.png" alt=""> 3 min ago</span>
                                <p>{{$value->isi}}</p>
                                <a href="/komentar/{{$value->id}}/edit" title="" class="active"><i class="fa"></i>edit</a>
                                @if ($value->is_liked_by_auth_user_comment())
                                    <a href="/komentar/unlike/{{$value->id}}"><i class="la la-heart ml-2"></i> Dislike {{$value->likes->count()}}</a>
                                    @else
                                    <a href="/komentar/like/{{$value->id}}"><i class="la la-heart ml-2"></i> Like {{$value->likes->count()}}</a>
                                @endif
                                {{-- <a href="#" title="" class="active"><i class="fa"></i>delete</a> --}}
                                <form action="/komentar/{{$value->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button class="bg-transparent border-0 hover" type="submit"><li><a>Delete</a></li></button>
                                </form>
                            </div>
                        </div><!--comment-list end-->
                    </li>
                    @empty
                    Tidak Ada Komentar<br><br>
                    
                @endforelse
            </ul>
        </div><!--comment-sec end-->
        <div class="post-comment">
            <div class="cm_img">
                <img src="http://via.placeholder.com/40x40" alt="">
            </div>
            <div class="comment_box">
                <form action="/komentar/create/{{$post->id}}" method="POST">
                    @csrf
                    <input type="text" name="isi" placeholder="Post a comment">
                    <button type="submit">Send</button>
                </form>
            </div>
        </div><!--post-comment end-->
    </div><!--comment-section end-->
</div><!--posty end-->
@endsection

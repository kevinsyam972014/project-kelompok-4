@extends('pages.master')
@section('content')
    <div class="post-project">
        <h3>Edit Postingan</h3>
        <div class="post-project-fields">
            <form action="/komentar/{{ $komentar->id }}/update" method="POST">
                @csrf
                @method('PUT')
                <div class="row">
                    
                    <div class="col-lg-12">
                    <textarea name="isi" class="form-control my-editor">{{strip_tags(old('isi', $komentar->isi))}}</textarea>
                    </div>

                    <div class="col-lg-12">
                        <ul>
                            <li><button class="active" type="submit">Edit Komentar</button></li>
                            <li><a href="/beranda">Cancel</a></li>
                        </ul>
                    </div>
                </div>
            </form>
@endsection
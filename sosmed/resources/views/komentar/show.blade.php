@extends('post.detail')
@section('show-komentar')
    <ul>
        @foreach ($komentator as $value)
        <li>
            <div class="comment-list">
                <div class="bg-img">
                    <img src="http://via.placeholder.com/40x40" alt="">
                </div>
                <div class="comment">
                    <h3>{{$value->coment->name}}</h3>
                    <span><img src="images/clock.png" alt=""> 3 min ago</span>
                    <p>{{$value->post->isi}}</p>
                    <a href="#" title="" class="active"><i class="fa fa-reply-all"></i>Reply</a>
                </div>
            </div><!--comment-list end-->
        </li>
        @endforeach
    </ul>
@endsection
    
